package com.juvetic.movieatuh;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juvetic on 5/9/2016.
 */
public class ImageAdapter extends BaseAdapter {

    private final List<String> urls = new ArrayList<>();
    private Context mContext;

    public ImageAdapter(Context mContext) {
        this.mContext = mContext;

        urls.add("http://i.imgur.com/C717mLm.png");
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public String getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageViewSquared view = (ImageViewSquared) convertView;

        if(view == null)
        {
            //Inisialisasi kalo gak recycled
            view = new ImageViewSquared(mContext);
            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        //Ambil url image dari posisi sekarang
        String url = getItem(position);

        //Trigger download dari URL ke imageview
        Picasso.with(mContext)
                .load(url)
                .fit()
                .tag(mContext)
                .into(view);

        return view;
    }
}